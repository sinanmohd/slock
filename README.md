# slock: simple screen locker

simple screen locker utility for X.

### patches

- [capscolor](https://tools.suckless.org/slock/patches/capscolor/slock-capscolor-20170106-2d2a21a.diff). introduces an additional color to indicate the state of caps Lock
- [Xresources](https://tools.suckless.org/slock/patches/xresources/slock-xresources-20191126-53e56c7.diff). adds the ability to get colors via Xresources
- [message](https://tools.suckless.org/slock/patches/message/slock-message-20191002-b46028b.diff). adds the ability to show message

### Xautolock

slock can be started after a specific period of user inactivity using xautolock. The command syntax is:

```console
xautolock -time 10 -locker slock
```

Simpler alternatives to xautolock might be [xssstate](https://git.suckless.org/xssstate/) or [xss](https://bitbucket.org/raymonad/xss-lock/src/master/).
