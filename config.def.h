/* user and group to drop privileges to */
static const char *user  = "nu";
static const char *group = "wheel";

static const char *colorname[NUMCOLS] = {
	[INIT] = "black",       /* after initialization */
	[INPUT] = "#005577",    /* during input */
	[FAILED] = "#CC3333",   /* wrong password */
	[CAPS] = "red",         /* CapsLock on */
};
/* text color */
static const char * text_color = "#ffffff";

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
		{ "color0",       STRING,  &colorname[INPUT] },
		{ "color2",       STRING,  &colorname[FAILED] },
		{ "color1",       STRING,  &colorname[CAPS] },
		{ "color8",       STRING,  &text_color },
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 0;

/* default message */
static const char * message = "Authentication required.";

/* text size (must be a valid size) */
static const char * font_name = "6x13";

